# Stripe App

Mini Proof-of-Concept du fonctionnement de Stripe sous la forme d’un petit shop en ligne.

Ce projet est composé de deux applications :

- Backend (Django) *stripe_app_backend*
- Frontend (Angular) *stripe-app-frontend*

## Installation des applications

Cloner le projet

    git clone https://gitlab.forge.hefr.ch/lucas.alborghe/stripe-app.git

 Créer l'environnement virtuel et installer les paquets du backend
 
    cd stripe_app_backend
    virtualenv -p python3 venv
    source venv/bin/activate
    pip install -r requirements.txt
    deactivate
    cd ..

Installer les paquets du frontend
 
    cd stripe-app-frontend
    npm install
    cd ..

## Démarrage des applications

Démarrage du backend (http://127.0.0.1:8000)

    cd stripe_app_backend
    source venv/bin/activate
    python manage.py runserver

Démarrage du frontend (http://localhost:4200/)

    cd stripe-app-frontend
    npm run start

## Stripe - Liens utiles

Dashboard : https://dashboard.stripe.com/test/dashboard

Cartes de test : https://stripe.com/docs/testing#cards

Documentation API : https://stripe.com/docs/api