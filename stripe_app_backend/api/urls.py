from rest_framework_extensions.routers import ExtendedSimpleRouter

from api import views

router = ExtendedSimpleRouter()

# =================================

router.register(r'merchants', views.MerchantViewSet, base_name='merchant') \
    .register(r'products', views.ProductViewSet, basename='merchant-products', parents_query_lookups=['merchant'])

router.register(r'sale', views.SaleCreateViewSet, base_name='sale')

# =================================

urlpatterns = router.urls

# =================================
