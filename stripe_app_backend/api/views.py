from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework_extensions.mixins import NestedViewSetMixin, DetailSerializerMixin

from api.serializers import MerchantSerializer, MerchantDetailSerializer, ProductSerializer, ProductDetailSerializer, \
    SaleCreateSerializer
from stripe_app.models import Product, Sale

User = get_user_model()


class MerchantViewSet(NestedViewSetMixin, DetailSerializerMixin, viewsets.ReadOnlyModelViewSet):
    serializer_class = MerchantSerializer
    serializer_detail_class = MerchantDetailSerializer
    queryset = User.objects.all()


class ProductViewSet(NestedViewSetMixin, DetailSerializerMixin, viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    serializer_detail_class = ProductDetailSerializer
    queryset = Product.objects.all()


class SaleCreateViewSet(viewsets.ModelViewSet):
    serializer_class = SaleCreateSerializer
    queryset = Sale.objects.all()
