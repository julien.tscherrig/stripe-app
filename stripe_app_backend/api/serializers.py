import json

import stripe
from django.contrib.auth import get_user_model
from rest_framework import serializers

from stripe_app.models import Product, Category, Sale, Client, State, ProductSale
from stripe_app_backend.settings import STRIPE_SECRET_KEY

User = get_user_model()


# =================================
# Others
# =================================

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client

        fields = [
            'id',
            'first_name',
            'last_name',
            'email'
        ]


class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State

        fields = [
            'id',
            'name'
        ]


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category

        fields = [
            'id',
            'name'
        ]


# =================================
# Merchant
# =================================

class MerchantSerializer(serializers.ModelSerializer):
    class Meta:
        model = User

        fields = [
            'id',
            'first_name',
            'last_name',
            'email',
            'date_joined',
            'is_superuser'
        ]


class MerchantDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User

        fields = [
            'id',
            'first_name',
            'last_name',
            'email',
            'date_joined',
            'is_superuser'
        ]


# =================================
# Product
# =================================

class ProductSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    merchant = MerchantSerializer(read_only=True)

    class Meta:
        model = Product

        fields = [
            'id',
            'name',
            'description',
            'image',
            'price',
            'is_available',
            'category',
            'merchant'
        ]


class ProductDetailSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    merchant = MerchantSerializer()

    class Meta:
        model = Product

        fields = [
            'id',
            'name',
            'description',
            'image',
            'price',
            'is_available',
            'category',
            'merchant'
        ]


# =================================
# Sale
# =================================

class SaleCreateSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(read_only=True)
    client = ClientSerializer()
    state = StateSerializer(read_only=True)
    products = ProductSerializer(write_only=True, many=True)
    token_id = serializers.CharField(write_only=True)

    class Meta:
        model = Sale
        fields = [
            'id',
            'date',
            'amount',
            'state',
            'client',
            'products',
            'token_id'
        ]

    def create(self, validated_data):
        # ----------------------------------------------------------------------
        # Create Stripe payment
        # ----------------------------------------------------------------------
        # Stripe API key
        stripe.api_key = STRIPE_SECRET_KEY
        token_id = validated_data.pop('token_id')
        stripe.Charge.create(
            amount=int(validated_data.get('amount') * 100),
            currency="EUR",
            source=token_id,  # obtained with Stripe.js
            description="Charge for " +
                        validated_data.get('client')['first_name'] + " " +
                        validated_data.get('client')['last_name']
        )
        # ----------------------------------------------------------------------
        # Create entries in database
        # ----------------------------------------------------------------------
        # Products sold
        products = validated_data.pop('products')
        # Client of these products
        new_client = Client.objects.create(**validated_data.pop('client'))
        # Sale
        new_sale = Sale.objects.create(**validated_data,
                                       state_id=1,
                                       client=new_client)
        # Create many to many entries
        for product in products:
            # ID is not available so using 'name' and 'price' to retrieve objects
            product_name = product['name']
            product_price = product['price']
            ProductSale.objects.create(sale=new_sale,
                                       product=Product.objects.get(name=product_name, price=product_price))
        return new_sale
