import django
from django.contrib.auth import models as auth_models

from django.db import models
from django.utils.timezone import now


class UserManager(auth_models.BaseUserManager):

    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password=password)
        user.is_superuser = user.is_staff = True
        user.save(using=self._db)
        return user


class User(auth_models.AbstractBaseUser, auth_models.PermissionsMixin):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    # add stripe_public_key of the shop
    # add stripe_private_key of the shop
    # add name of the shop

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'Manager'
        verbose_name_plural = 'Managers'


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=1024)
    image = models.CharField(max_length=250)
    price = models.FloatField()
    is_available = models.BooleanField()
    category = models.ForeignKey(Category, on_delete=models.PROTECT, related_name='products')
    merchant = models.ForeignKey(User, on_delete=models.PROTECT, related_name='products')

    def __str__(self):
        return self.name + ' (Category : ' + self.category.name + ')'


class State(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Client(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class Sale(models.Model):
    date = models.DateTimeField(default=now)
    amount = models.FloatField()
    state = models.ForeignKey(State, on_delete=models.PROTECT, related_name='sales')
    client = models.ForeignKey(Client, on_delete=models.PROTECT, related_name='sales')

    def __str__(self):
        return str(self.date) + ' (' + str(self.amount) + '$) [' + self.state.name + ']'


class ProductSale(models.Model):
    product = models.ForeignKey(Product, on_delete=models.PROTECT, related_name='product_sale')
    sale = models.ForeignKey(Sale, on_delete=models.PROTECT, related_name='product_sale')

    def __str__(self):
        return '[' + str(self.sale.date) + '] ' + self.product.name
