from django.contrib import admin
from django.contrib.admin import register

from django.contrib.auth import admin as auth_admin

from .models import *
from .form import UserChangeForm, UserCreationForm


# -----------------------------------------------
# Sales
# -----------------------------------------------

class ProductSaleInline(admin.TabularInline):
    model = ProductSale
    extra = 0


@register(Sale)
class SaleAdmin(admin.ModelAdmin):
    inlines = [ProductSaleInline]


# -----------------------------------------------
# Categories
# -----------------------------------------------

@register(Category)
class CategoryAdmin(admin.ModelAdmin):

    def has_module_permission(self, request):
        return True

    def has_add_permission(self, request):
        return True

    def has_view_or_change_permission(self, request, obj=None):
        return True


# -----------------------------------------------
# Products
# -----------------------------------------------

@register(Product)
class ProductAdmin(admin.ModelAdmin):

    def has_module_permission(self, request):
        return True

    def has_add_permission(self, request):
        return True

    def has_view_or_change_permission(self, request, obj=None):
        return True


# -----------------------------------------------
# States
# -----------------------------------------------

@register(State)
class StateAdmin(admin.ModelAdmin):
    pass


# -----------------------------------------------
# Clients
# -----------------------------------------------

@register(Client)
class ClientAdmin(admin.ModelAdmin):
    pass


# -----------------------------------------------
# Clients
# -----------------------------------------------

@register(User)
class UserAdmin(auth_admin.UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser',
                                    'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )
    limited_fieldsets = (
        (None, {'fields': ('email',)}),
        ('Personal info', {'fields': ('first_name', 'last_name')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
         ),
    )
    form = UserChangeForm
    add_form = UserCreationForm
    change_password_form = auth_admin.AdminPasswordChangeForm
    list_display = ('email', 'first_name', 'last_name', 'is_superuser')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('first_name', 'last_name', 'email')
    ordering = ('email',)
    readonly_fields = ('last_login', 'date_joined',)
