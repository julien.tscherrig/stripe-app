import {Component} from '@angular/core';
import {Subscription} from 'rxjs';
import {ItemsInCartService} from './services/items-in-cart/items-in-cart.service';
import {environment} from '../environments/environment';
import {Product} from './models/model_product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'stripe-app-frontend';
  itemsInCart: number;
  subscription: Subscription;

  constructor(private  itemsInCartService: ItemsInCartService) {
    // Get on init
    const productsList = JSON.parse(localStorage.getItem(environment.cart_key)) as Product[];
    if (productsList) {
      this.itemsInCart = productsList.length;
    } else {
      this.itemsInCart = 0;
    }
    // Get update
    this.subscription = this.itemsInCartService.getItemsInCart().subscribe(response => {
      if (response) {
        this.itemsInCart = response.itemsInCart;
      }
    });
  }

}
