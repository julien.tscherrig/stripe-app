import {Injectable} from '@angular/core';
import {ApiService} from '../api.service';
import {Observable} from 'rxjs';
import {Product} from '../../models/model_product';
import {Merchant} from '../../models/model_merchant';
import {Sale} from '../../models/model_sale';

const API_URL = 'api/';

@Injectable({
  providedIn: 'root'
})
export class StripeService {

  constructor(public readonly  apiService: ApiService) {
  }

  // ====================================================================================
  // Stripe app related API calls
  // ====================================================================================

  getAllMerchants(): Observable<Merchant[]> {
    return this.apiService.get(API_URL + 'merchants/');
  }

  getMerchantProducts(merchantId: number): Observable<Product[]> {
    return this.apiService.get(API_URL + 'merchants/' + merchantId + '/products/');
  }

  createNewSale(newSale: Sale): Observable<Sale> {
    return this.apiService.post(API_URL + 'sale/', newSale);
  }

}
