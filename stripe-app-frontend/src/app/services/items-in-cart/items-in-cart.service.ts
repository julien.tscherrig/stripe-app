import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ItemsInCartService {

  private subject = new Subject<any>();

  sendItemsInCart(nb: number) {
    this.subject.next({itemsInCart: nb});
  }

  getItemsInCart(): Observable<any> {
    return this.subject.asObservable();
  }

}
