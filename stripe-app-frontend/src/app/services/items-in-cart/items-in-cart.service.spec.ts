import { TestBed } from '@angular/core/testing';

import { ItemsInCartService } from './items-in-cart.service';

describe('ItemsInCartService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ItemsInCartService = TestBed.get(ItemsInCartService);
    expect(service).toBeTruthy();
  });
});
