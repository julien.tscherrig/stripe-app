import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {environment} from '../../environments/environment';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(public http: HttpClient) {
  }

  // ====================================================================================
  // Common GET/POST/PUT/DELETE API calls
  // ====================================================================================

  public get(path: string): Observable<any[]> {
    return this.http.get<any[]>(API_URL + path, {headers: this.getHttpHeaders()})
      .pipe(
        tap(() => this.log('fetched objects')),
        catchError(this.handleError<any[]>('get', []))
      );
  }

  public post(path: string, body: Object = {}): Observable<any> {
    return this.http.post<any>(API_URL + path, JSON.stringify(body), {headers: this.getHttpHeaders()})
      .pipe(
        tap((newObject: Object) => this.log(`added`)),
        catchError(this.handleError<any>('post'))
      );
  }

  public put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(API_URL + path, JSON.stringify(body), {headers: this.getHttpHeaders()})
      .pipe(
        tap((newObject: Object) => this.log(`updated`)),
        catchError(this.handleError<any>('put'))
      );
  }

  public delete(path: string): Observable<any> {
    return this.http.delete<any>(API_URL + path, {headers: this.getHttpHeaders()})
      .pipe(
        tap(() => this.log(`deleted`)),
        catchError(this.handleError<any>('delete'))
      );
  }

  // ====================================================================================
  // HttpHeaders, errors and logs handlers
  // ====================================================================================

  public getHttpHeaders() {
    return new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  public handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  public log(message: string) {
    console.log(`Log: ${message}`);
  }

}
