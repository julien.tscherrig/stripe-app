import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './pages/home/home.component';
import {ProductsListComponent} from './pages/products-list/products-list.component';
import {CartComponent} from './pages/cart/cart.component';
import {OrderComponent} from './pages/order/order.component';
import {OrderOkComponent} from './pages/order-ok/order-ok.component';

const routes: Routes = [
  // Home pag
  {path: '', component: HomeComponent},
  // Products list page
  {path: 'merchant/:mId', component: ProductsListComponent},
  // Cart page
  {path: 'cart', component: CartComponent},
  // Order page
  {path: 'order', component: OrderComponent},
  // Order OK page
  {path: 'order/success', component: OrderOkComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
