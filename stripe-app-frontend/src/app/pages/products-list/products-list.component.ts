import {Component, OnInit} from '@angular/core';
import {StripeService} from '../../services/stripe/stripe.service';
import {Product} from '../../models/model_product';
import {ActivatedRoute} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ItemsInCartService} from '../../services/items-in-cart/items-in-cart.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  public currentMerchantId: number;
  public productsList: Product[];

  constructor(public stripeService: StripeService,
              private  itemsInCartService: ItemsInCartService,
              private sanitizer: DomSanitizer,
              public route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.currentMerchantId = params.mId;
    });
  }

  ngOnInit() {
    this.stripeService.getMerchantProducts(this.currentMerchantId).subscribe(response => {
      console.log(response);
      this.productsList = response;
    });
  }

  getImage(url) {
    return this.sanitizer.bypassSecurityTrustStyle('url(' + url + ')');
  }

  addToCart(productToCart: Product) {
    // Add product to cart
    let products = JSON.parse(localStorage.getItem(environment.cart_key));
    if (products == null || products.length === 0) {
      // Create cart and add item
      products = [Product];
      products[0] = productToCart;
    } else {
      // Add item
      const newIndex = products.length;
      products[newIndex] = productToCart;
    }
    localStorage.setItem(environment.cart_key, JSON.stringify(products));
    // New products list
    const newProducts = JSON.parse(localStorage.getItem(environment.cart_key)) as Product[];
    console.log(newProducts);
    this.itemsInCartService.sendItemsInCart(newProducts.length);
  }

}
