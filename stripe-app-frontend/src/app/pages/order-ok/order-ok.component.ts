import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-order-ok',
  templateUrl: './order-ok.component.html',
  styleUrls: ['./order-ok.component.css']
})
export class OrderOkComponent {

  constructor(public router: Router) {
  }

  backMenu() {
    this.router.navigate(['/']);
  }

}
