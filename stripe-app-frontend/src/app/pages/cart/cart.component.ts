import {Component, OnInit} from '@angular/core';
import {Product} from '../../models/model_product';
import {StripeService} from '../../services/stripe/stripe.service';
import {DomSanitizer} from '@angular/platform-browser';
import {ItemsInCartService} from '../../services/items-in-cart/items-in-cart.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public productsList: Product[];

  constructor(private  itemsInCartService: ItemsInCartService, private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.productsList = JSON.parse(localStorage.getItem(environment.cart_key));
    console.log(this.productsList);
  }

  getImage(url) {
    return this.sanitizer.bypassSecurityTrustStyle('url(' + url + ')');
  }

  cleanCart() {
    this.productsList = null;
    localStorage.removeItem(environment.cart_key);
    this.itemsInCartService.sendItemsInCart(0);
  }

}
