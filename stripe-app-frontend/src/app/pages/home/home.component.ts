import {Component, OnInit} from '@angular/core';
import {Merchant} from '../../models/model_merchant';
import {StripeService} from '../../services/stripe/stripe.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public merchantsList: Merchant[];

  constructor(public stripeService: StripeService, private sanitizer: DomSanitizer, public router: Router) {
  }

  ngOnInit() {
    this.stripeService.getAllMerchants().subscribe(response => {
      console.log(response);
      this.merchantsList = response;
    });
  }

  getImage(url) {
    return this.sanitizer.bypassSecurityTrustStyle('url(' + url + ')');
  }

  merchantProducts(id: number) {
    this.router.navigate(['merchant', id]);
  }
}
