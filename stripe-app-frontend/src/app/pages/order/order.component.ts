import {Component, OnInit} from '@angular/core';
import {Product} from '../../models/model_product';
import {FormControl} from '@angular/forms';
import {environment} from '../../../environments/environment';
import {StripeService} from '../../services/stripe/stripe.service';
import {Sale} from '../../models/model_sale';
import {Router} from '@angular/router';
import {ItemsInCartService} from '../../services/items-in-cart/items-in-cart.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  public productsList: Product[];
  public totalPrice: number;

  public formFirstname = new FormControl('');
  public formLastname = new FormControl('');
  public formAddress = new FormControl('');

  public newSale: Sale;

  constructor(public stripeService: StripeService, private  itemsInCartService: ItemsInCartService, public router: Router) {
  }

  ngOnInit() {
    this.productsList = JSON.parse(localStorage.getItem(environment.cart_key)) as Product[];
    console.log(this.productsList);
    // Get total price
    this.totalPrice = 0;
    for (var i = 0; i < this.productsList.length; i++) {
      this.totalPrice += +this.productsList[i].price;
    }
  }

  openCheckout() {
    var handler = (<any> window).StripeCheckout.configure({
      key: 'pk_test_hkKubYY3daYB1NZkRCbHYUFi00Z5dA5xYJ',
      locale: 'auto',
      token: (token: any) => {
        console.log('Token ID : ' + token.id);
        console.log(token);
        // New sale
        this.newSale = {
          amount: this.totalPrice,
          state: {
            id: 1,
            name: 'Waiting'
          },
          client: {
            first_name: this.formFirstname.value,
            last_name: this.formLastname.value,
            email: token.email
          },
          products: this.productsList,
          token_id: token.id
        };
        console.log(this.newSale);
        // Create new sale
        this.stripeService.createNewSale(this.newSale).subscribe(response => {
          if (response) {
            this.cleanCart();
            this.router.navigate(['order', 'success']);
          } else {
            alert('[-] Error while processing order !');
          }
        });
      }
    });

    handler.open({
      name: 'HumanTech Online Shop',
      description: 'Payment information',
      amount: this.totalPrice * 100,
      currency: 'EUR'
    });
  }

  cleanCart() {
    this.productsList = null;
    localStorage.removeItem(environment.cart_key);
    this.itemsInCartService.sendItemsInCart(0);
  }

}
