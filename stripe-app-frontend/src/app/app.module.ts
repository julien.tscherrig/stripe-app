import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HomeComponent} from './pages/home/home.component';
import {ProductsListComponent} from './pages/products-list/products-list.component';
import {CartComponent} from './pages/cart/cart.component';
import {ApiService} from './services/api.service';
import {StripeService} from './services/stripe/stripe.service';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {OrderComponent} from './pages/order/order.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { OrderOkComponent } from './pages/order-ok/order-ok.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductsListComponent,
    CartComponent,
    OrderComponent,
    OrderOkComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ApiService,
    StripeService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
