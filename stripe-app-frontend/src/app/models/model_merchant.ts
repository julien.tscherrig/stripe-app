export class Merchant {
  id?: number;
  first_name: string;
  last_name: string;
  email: string;
  date_joined: string;
  is_superuser: boolean;
}
