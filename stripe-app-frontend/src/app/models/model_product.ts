import {Category} from './model_category';

export class Product {
  id?: number;
  name: string;
  description: string;
  image: string;
  price: string;
  is_available: string;
  category: Category;
  merchant: string;
}
