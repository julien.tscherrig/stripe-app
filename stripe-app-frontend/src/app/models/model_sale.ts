import {State} from './model_state';
import {Client} from './model_client';
import {Product} from './model_product';

export class Sale {
  id?: number;
  date?: Date;
  amount: number;
  state: State;
  client: Client;
  products?: Product[];
  token_id?: string;
}
